package com.buisyday.happybirth.presenter.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.buisyday.happybirth.R
import com.buisyday.happybirth.view.BirthdayFragment
import com.bumptech.glide.Glide

class BusyDaysAdapter(
    private val mContext: Context,
    private val busyList: MutableList<BirthdayFragment.BusyBookDay>
) : BaseAdapter() {

    override fun getCount(): Int {
        return busyList.size
    }

    override fun getItem(position: Int): Any? {
        return busyList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        val grid = if (convertView == null) {
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.card_busy_day, parent, false)
        } else {
            convertView
        }
        val imageView = grid.findViewById<View>(R.id.avatar) as ImageView
        val textDateView = grid.findViewById<View>(R.id.date_text) as TextView
        val textNameView = grid.findViewById<View>(R.id.name_text) as TextView
        val topCard = grid.findViewById<View>(R.id.top_card) as ConstraintLayout
        val bottomCard = grid.findViewById<View>(R.id.bottom_card) as ConstraintLayout
        Glide.with(mContext)
            .load(busyList[position].avatar)
            .placeholder(R.drawable.ic_launcher)
            .circleCrop()
            .into(imageView)
        textDateView.text = busyList[position].date
        textNameView.text = busyList[position].name
        if (busyList[position].busyDay){
            topCard.background = mContext.resources.getDrawable(R.drawable.main_card)
            bottomCard.background = mContext.resources.getDrawable(R.drawable.main_subcard)
        }else{
            topCard.background = mContext.resources.getDrawable(R.drawable.second_card)
            bottomCard.background = mContext.resources.getDrawable(R.drawable.second_subcard)
        }
        grid.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.withAppendedPath(
                ContactsContract.Contacts.CONTENT_URI,
                busyList[position].id
            )
            intent.data = uri
            mContext.startActivity(intent)

        }
        return grid
    }
}
