package com.buisyday.happybirth.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.buisyday.happybirth.R
import com.buisyday.happybirth.model.entity.DayModel
import com.buisyday.happybirth.presenter.adapters.BusyDaysAdapter
import kotlinx.android.synthetic.main.birthday_fragment.*
import java.text.SimpleDateFormat
import java.util.*

class BirthdayFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.birthday_fragment, container, false)
    }

    fun showEvents(eventSort: List<DayModel>) {
        progress_bar.visibility = View.GONE
        text_progress.visibility = View.GONE
        text_progress2.visibility = View.GONE
        val simpleDate = SimpleDateFormat("dd.MM.yyyy")
        val currentTime: Date = Calendar.getInstance().time
        val busyList =
            eventSort.map {
                BusyBookDay(
                    it.photoUri,
                    it.personName.split(" ").first(),
                    simpleDate.format(it.date),
                    it.id,
                    simpleDate.format(it.date) == simpleDate.format(currentTime)
                )
            }
        if (busyList.isEmpty()) {
            grid_busy_days.visibility = View.GONE
            placeholder_img.visibility = View.VISIBLE
        } else {
            grid_busy_days.visibility = View.VISIBLE
            placeholder_img.visibility = View.GONE
            val adapter = BusyDaysAdapter(context!!, busyList.toMutableList())
            grid_busy_days.adapter = adapter
            grid_busy_days.expanded = true
        }
    }

    data class BusyBookDay(
        val avatar: String,
        val name: String,
        val date: String,
        val id: String,
        val busyDay: Boolean
    )
}