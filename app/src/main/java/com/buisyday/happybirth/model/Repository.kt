package com.buisyday.happybirth.model

import android.annotation.SuppressLint
import android.content.Context
import com.buisyday.happybirth.model.entity.DayModel

@SuppressLint("StaticFieldLeak")
object Repository {

    lateinit var context: Context

    fun initRepository(context: Context) {
        Repository.context = context
    }

    fun insertEventContacts(eventContact: List<DayModel>) {
        for (event in eventContact) {
            AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.insertEventContact(event)
        }
    }

    fun getAllEvents(): List<DayModel> {
        return AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.getAllEvents()
    }

}