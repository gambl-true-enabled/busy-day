package com.buisyday.happybirth.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.buisyday.happybirth.model.dao.DaoEventContacts
import com.buisyday.happybirth.model.entity.DayModel


@Database(
    entities = [DayModel::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun eventContactsDao(): DaoEventContacts


    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    val nameDB = "dbBusyDay"
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        nameDB
                    ).build()
                }
            }
            return INSTANCE
        }
    }
}
