package com.buisyday.happybirth.model.dao

import androidx.room.*
import com.buisyday.happybirth.model.entity.DayModel

@Dao
interface DaoEventContacts {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventContact(EventContact: DayModel)

    @Query("SELECT * FROM daymodel")
    fun getAllEvents(): List<DayModel>

}


