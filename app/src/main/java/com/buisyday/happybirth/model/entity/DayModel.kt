package com.buisyday.happybirth.model.entity

import androidx.room.*
import com.buisyday.happybirth.model.converters.ConverterDate
import com.buisyday.happybirth.utilki.getMonthByNumber
import java.text.SimpleDateFormat
import java.util.*

@TypeConverters(ConverterDate::class)
@Entity
data class DayModel(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "personName")val personName: String = "",
    @ColumnInfo(name = "dayName")val dayName: String = "",
    @ColumnInfo(name = "date") val date: Date = Date(),
    @ColumnInfo(name = "photoUri")val photoUri: String = "",
    @ColumnInfo(name = "numberPhone")val numberPhone: String = ""
) {
    fun getDataString(): String {
        val day = SimpleDateFormat("dd", Locale.getDefault()).format(date)
        val month = SimpleDateFormat("MM", Locale.getDefault()).format(date)
        return "$day ${getMonthByNumber(month.toInt())}"
    }
}
