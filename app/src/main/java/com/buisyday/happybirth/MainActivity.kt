package com.buisyday.happybirth

import android.Manifest

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.buisyday.happybirth.model.Repository
import com.buisyday.happybirth.view.BirthdayFragment
import com.buisyday.happybirth.presenter.ContactsPresenter
import kotlinx.android.synthetic.main.activity_birthday.*


const val PERMISSION_REQUEST_CODE = 114

class MainActivity : AppCompatActivity() {

    private lateinit var mainFragment: BirthdayFragment
    private lateinit var contactPresenter: ContactsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_birthday)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        Repository.initRepository(applicationContext)
        mainFragment = BirthdayFragment()
        contactPresenter = ContactsPresenter(mainFragment)

        button_permission.setOnClickListener {
            requestPermissionOrShowContacts()
        }
        requestPermissionOrShowContacts()
    }

    private fun requestPermissionOrShowContacts() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSION_REQUEST_CODE
            )
        } else {
            showContacts()
        }
    }

    private fun showContacts() {
        permission_not_granted.visibility = View.GONE
        contactPresenter.initContacts(contentResolver)
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, mainFragment)
            .commit()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                showContacts()
            }
        }
    }
}